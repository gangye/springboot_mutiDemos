# springboot_mutiDemos

sleuth-ownconfig-loglink分支中
springboot_demoA是使用spring-cloud-sleuth的方式在日志中展示了traceId和spanId
springboot_demoB是使用MDC+过滤器的方式在日志中展示了traceId和spanId
common_demo是公共部分代码，供springboot_demoA和springboot_demoB引用


发行版版本说明：
v1.0是普通的链路调度跟踪，可以接受上游，但是无法传递到下游
v1.1实现RestTemplate和okhttp请求时的日志链路跟踪
v1.2实现多线程中线程池的情形下日志链路的跟踪