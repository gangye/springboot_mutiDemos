package com.csrcb;

import com.csrcb.filter.ThreadPoolExecutorMdcUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Classname AppBStart
 * @Description springboot_demoB的启动类
 * @Date 2021/8/17 9:50
 * @Created by gangye
 */
@SpringBootApplication
public class AppBStart {
    public static void main(String[] args) {
        SpringApplication.run(AppBStart.class,args);
    }

    @Value("${task.pool.corePoolSize}")
    private int corePoolSize;
    @Value("${task.pool.maxPoolSize}")
    private int maxPoolSize;
    @Value("${task.pool.keepAliveSeconds}")
    private int keepAliveSeconds;
    @Value("${task.pool.queueCapacity}")
    private int queueCapacity;

    @Bean(name = "myAsyncThreadPool")
    public ThreadPoolTaskExecutor myAsyncThreadPool(){
        ThreadPoolTaskExecutor pool = new ThreadPoolExecutorMdcUtil();
        //核心线程池大小
        pool.setCorePoolSize(corePoolSize);
        //最大线程数
        pool.setMaxPoolSize(maxPoolSize);
        //队列容量
        pool.setQueueCapacity(queueCapacity);
        //活跃时间
        pool.setKeepAliveSeconds(keepAliveSeconds);
        //线程池的名字前缀
        pool.setThreadNamePrefix("myAsyncExcutor-");
        //拒绝策略
        pool.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        pool.initialize();
        return pool;
    }
}
