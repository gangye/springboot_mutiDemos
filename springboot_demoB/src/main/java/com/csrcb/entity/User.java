package com.csrcb.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname User
 * @Date 2020/9/2 14:51
 * @Created by gangye
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;
    private String name;
    private String address;

    public User(Integer id) {
        this.id = id;
    }
}
