package com.csrcb.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @Classname ParallelServeces
 * @Description 使用多线程的日志链路跟踪
 * @Date 2021/8/20 10:53
 * @Created by gangye
 */
@Slf4j
@Service
public class ParallelServeces {

    @Autowired
    private AssetService assetService;

    @Autowired
    @Qualifier(value = "myAsyncThreadPool")
    private ThreadPoolTaskExecutor myAsyncThreadPool;

    public Map<String ,Object> aclcCustAssetInfo(int custId) throws IOException {
        log.info("获取用户{}的总资产----------start---------",custId);
        Map<String, Object> assetMap = new HashMap<>();
        BigDecimal totalVal = BigDecimal.ZERO;
        Future<BigDecimal> acctValFuture = myAsyncThreadPool.submit(() -> {return assetService.getAcctBal(custId);});
        Future<BigDecimal> finacValFuture = myAsyncThreadPool.submit(() -> {return assetService.getFinacVal();});
        Future<BigDecimal> fundValFuture = myAsyncThreadPool.submit(() -> {return assetService.getFundMoneyVal();});
        BigDecimal acctVal = BigDecimal.ZERO;
        BigDecimal finacVal = BigDecimal.ZERO;
        BigDecimal fundVal = BigDecimal.ZERO;
        try {
            //账户余额
            acctVal = acctVal.add(acctValFuture.get());
            //理财份额
            finacVal = finacVal.add(finacValFuture.get());
            //基金份额
            fundVal = fundVal.add(fundValFuture.get());
        } catch (InterruptedException e) {
            log.error("线程池阻塞异常",e);
        } catch (ExecutionException e) {
            log.error("线程池执行线程异常",e);
        }
        totalVal = totalVal.add(acctVal).add(finacVal).add(fundVal);
        assetMap.put("total",totalVal);
        log.info("获取用户{}的总资产----------end---------",custId);
        return assetMap;
    }
}
