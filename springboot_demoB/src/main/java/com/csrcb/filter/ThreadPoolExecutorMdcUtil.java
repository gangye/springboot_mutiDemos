package com.csrcb.filter;

import org.slf4j.MDC;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.*;

/**
 * @Classname ThreadPoolExecutorMdcUtil
 * @Description 将MDC写入多线程的线程池工具，实例化的时候需要无参构造然后set
 * @Date 2021/8/20 17:50
 * @Created by gangye
 */
public class ThreadPoolExecutorMdcUtil extends ThreadPoolTaskExecutor {

    //重写执行方法，将MDC塞入
    @Override
    public void execute(Runnable task) {
        super.execute(ThreadPoolMDCFilter.wrap(task, MDC.getCopyOfContextMap()));
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        return super.submit(ThreadPoolMDCFilter.wrap(task, MDC.getCopyOfContextMap()));
    }

    @Override
    public Future<?> submit(Runnable task) {
        return super.submit(ThreadPoolMDCFilter.wrap(task, MDC.getCopyOfContextMap()));
    }
}
