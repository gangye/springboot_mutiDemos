package com.csrcb.controller;

import com.csrcb.entity.User;
import com.csrcb.services.UserService;
import com.csrcb.tools.ErrorEnum;
import com.csrcb.tools.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @Classname UserController
 * @Date 2021年7月30日10:05:57
 * @Created by gangye
 */
@RestController
@RequestMapping(value = "/api/springbootDemoB/user")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/getOneUser")
    public Response getOneUser(@RequestBody User user){
        Response response = Response.newResponse();
        response.setData(userService.getOneUser(user));
        return response;
    }

    @PostMapping("/getAllUser")
    public Response getAllUser(){
        Response response = Response.newResponse();
        response.setData(userService.getUserList());
        return response;
    }

    @PostMapping("/getDemoAOneUser")
    //在service中，通过resttemplate调用springboot_demoA获取信息，查看日志
    public Response getDemoAOneUser(@RequestBody User user){
        Response response = Response.newResponse();
        try {
            response.setData(userService.getDemoAUserInfo(user));
        } catch (IOException e) {
            log.error("json格式转化失败",e);
            response.setError(ErrorEnum.JSON_FORMAT_FAIL);
        }
        return response;
    }
}
