package com.csrcb.controller;

import com.csrcb.entity.User;
import com.csrcb.services.ParallelServeces;
import com.csrcb.tools.ErrorEnum;
import com.csrcb.tools.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @Classname ParallerController
 * @Description 使用多线程计算的
 * @Date 2021/8/20 16:22
 * @Created by gangye
 */
@Slf4j
@RestController
@RequestMapping("/api/springbootDemoB/acct")
public class ParallerController {
    @Autowired
    private ParallelServeces parallelServeces;

    @PostMapping("/getAllAcctVal")
    public Response getAllAcctVal(@RequestBody User user){
        Response response = Response.newResponse().OK();
        try {
            response.setData(parallelServeces.aclcCustAssetInfo(user.getId()));
        } catch (IOException e) {
            log.error("json格式转化失败",e);
            response.setError(ErrorEnum.JSON_FORMAT_FAIL);
        }
        return response;
    }
}
