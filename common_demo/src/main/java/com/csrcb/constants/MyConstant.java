package com.csrcb.constants;

/**
 * @Classname MyConstant
 * @Description 常量值的类
 * @Date 2021/8/16 10:05
 * @Created by gangye
 */
public class MyConstant {
    /**
     * 日志跟踪标识
     */
    public static final String TRACE_ID_MDC_FIELD = "traceId";
    public static final String SPAN_ID_MDC_FIELD = "spanId";
    public static final String PARENT_SPAN_ID_MDC_FIELD = "parentSpanId";
    public static final String TRACE_ID_HTTP_FIELD = "X-B3-TraceId";
    public static final String SPAN_ID_HTTP_FIELD = "X-B3-SpanId";
    public static final String PARENT_SPAN_ID_HTTP_FIELD = "X-B3-ParentSpanId";
    public static final String SAMPLED_HTTP_FIELD = "X-B3-Sampled";

    /**
     * http请求头
     */
    public static final String APPLICATION_JSON_UTF8 = "application/json;charset=UTF-8";

}
