package com.csrcb.controller;

import com.csrcb.entity.User;
import com.csrcb.service.AcctService;
import com.csrcb.tools.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname AcctInfoController
 * @Date 2021年8月20日14:31:33
 * @Created by gangye
 */
@RestController
@RequestMapping(value = "/api/springbootDemoA/account")
public class AcctInfoController {
    @Autowired
    private AcctService acctService;

    @PostMapping("/getAcctInfoByUserId")
    public Response getOneUser(@RequestBody User user){
        Response response = Response.newResponse();
        response.setData(acctService.getUserAcctInfo(user.getId()));
        return response;
    }

}
