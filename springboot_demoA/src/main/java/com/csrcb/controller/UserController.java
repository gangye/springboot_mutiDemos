package com.csrcb.controller;

import com.csrcb.entity.User;
import com.csrcb.service.UserService;
import com.csrcb.tools.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname UserController
 * @Date 2021年7月30日10:05:57
 * @Created by gangye
 */
@RestController
@RequestMapping(value = "/api/springbootDemoA/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/getOneUser")
    public Response getOneUser(@RequestBody User user){
        Response response = Response.newResponse();
        response.setData(userService.getOneUser(user));
        return response;
    }

    @PostMapping("/getAllUser")
    public Response getAllUser(){
        Response response = Response.newResponse();
        response.setData(userService.getUserList());
        return response;
    }
}
