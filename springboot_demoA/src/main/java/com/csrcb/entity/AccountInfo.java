package com.csrcb.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Classname AccountInfo
 * @Description 账户信息
 * @Date 2021/8/20 13:40
 * @Created by gangye
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountInfo {
    private Integer userId;//用户id
    private String acctName;//账户名
    private String acctNo;//账户号
    private BigDecimal avlBal;//余额
}
