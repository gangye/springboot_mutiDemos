package com.csrcb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Classname AppAStart
 * @Description springboot_demoA的启动类
 * @Date 2021/8/17 10:31
 * @Created by gangye
 */
@SpringBootApplication
public class AppAStart {
    public static void main(String[] args) {
        SpringApplication.run(AppAStart.class,args);
    }
}
