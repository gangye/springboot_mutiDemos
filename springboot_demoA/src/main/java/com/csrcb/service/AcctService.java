package com.csrcb.service;

import com.csrcb.entity.AccountInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Classname AcctService
 * @Date 2021/8/20 13:44
 * @Created by gangye
 */
@Slf4j
@Service
public class AcctService {
    private static List<AccountInfo> initAllAcctNos(){
        AccountInfo accountInfo1 = new AccountInfo(1,"张三","62166111",new BigDecimal("231432.12"));
        AccountInfo accountInfo2 = new AccountInfo(2,"李四","62166112",new BigDecimal("12312.88"));
        AccountInfo accountInfo3 = new AccountInfo(3,"王五","62166113",new BigDecimal("3500"));
        AccountInfo accountInfo4 = new AccountInfo(4,"赵六","62166114",new BigDecimal("8000.00"));
        AccountInfo accountInfo5 = new AccountInfo(1,"张三","66166124",new BigDecimal("2000.00"));
        return Arrays.asList(accountInfo1,accountInfo2,accountInfo3,accountInfo4,accountInfo5);
    }

    public List<AccountInfo> getUserAcctInfo(int userId){
        List<AccountInfo> accountInfos = initAllAcctNos();
        log.info("需要统计的用户id为: {}",userId);
        List<AccountInfo> accts = accountInfos.stream().filter(a -> userId == a.getUserId().intValue()).collect(Collectors.toList());
        if (!accts.isEmpty()){
            log.info("筛选满足用户id为: {}的账户信息: {}",userId, accts);
            return accts;
        }
        return null;
    }
}
