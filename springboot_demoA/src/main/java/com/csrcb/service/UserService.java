package com.csrcb.service;

import com.csrcb.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname UserService
 * @Date 2021年7月30日10:05:40
 * @Created by gangye
 */
@Service
@Slf4j
public class UserService {
    public User getOneUser(User user){
        log.info("请求的user信息{}",user);
        log.info("debug断点调试使用日志");
        Integer id = user.getId();
        user.setId(id);
        user.setName("demoA张三");
        user.setAddress("苏州");
        log.info("返回的user信息{}",user);
        return user;
    }

    public List<User> getUserList(){
        List<User> users = new ArrayList<>();
        User user1 = new User(1,"张三","苏州");
        User user2 = new User(2,"李四","上海");
        User user3 = new User(3,"王五","北京");
        users.add(user1);
        users.add(user2);
        users.add(user3);
        return users;
    }
}
